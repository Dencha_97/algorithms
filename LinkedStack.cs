﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Algorithms
{
	public class LinkedStack<T> : IEnumerable<T>
	{
		private readonly List.SinglyLinkedList<T> _list = new List.SinglyLinkedList<T>();

		public double Count => _list.Count;
		public bool IsEmpty => Count == 0;

		public T Peek()
		{
			if (IsEmpty)
				throw new InvalidOperationException();
			return _list.Head.Value;
		}

		public void Pop()
		{
			if (IsEmpty)
				throw new InvalidOperationException();
			_list.RemoveFirst();
		}

		public void Push(T item)
		{
			_list.AddFirst(item);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
