﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Algorithms
{
	public class Sorting
	{
		public Sorting() { }

		/// <summary>
		/// смена мест элементов
		/// </summary>
		/// <param name="array"></param>
		/// <param name="i"></param>
		/// <param name="j"></param>
		static void Swap(int[] array, int i, int j)
		{
			if (i == j) return;

			int temp = array[i];
			array[i] = array[j];
			array[j] = temp;

		}

		/// <summary>
		/// вывод на экран отсортированный массив
		/// </summary>
		/// <param name="array">массив</param>
		void GetArray(int[] array)
		{
			foreach (var i in array)
			{
				Console.Write(i + " ");
			}
		}

		/// <summary>
		/// Пузырьковая сортировка
		/// </summary>
		/// <param name="array">массив</param>
		public static void BubbleSort(int[] array)
		{
			for (int PartIndex = array.Length - 1; PartIndex > 0; PartIndex--)
			{
				for (int i = 0; i < PartIndex; i++)
				{
					if (array[i] > array[i + 1])
					{
						Swap(array, i, i + 1);
					}
				}
			}
		}

		/// <summary>
		/// Сортировка выборкой
		/// </summary>
		/// <param name="array">массив</param>
		public static void SelectionSort(int[] array)
		{
			for (int partIndex = array.Length - 1; partIndex > 0; partIndex--)
			{
				int largest = 0;
				for (int i = 1; i <= partIndex; i++)
				{
					if (array[i] > array[largest])
					{
						largest = i;
					}
				}
				Swap(array, largest, partIndex);
			}
		}

		/// <summary>
		/// Сортировка вставкой
		/// </summary>
		/// <param name="array">массив</param>
		public static void InsertionSort(int[] array)
		{
			for (int partindex = 1; partindex < array.Length; partindex++)
			{
				int cursor = array[partindex]; //сохранить текущий не отсортированный элемент массива
				int i = 0;

				for (i = partindex; i > 0 && array[i - 1] > cursor; i--)
				{
					array[i] = array[i - 1];
				}

				array[i] = cursor;
			}
		}

		/// <summary>
		/// сортировка Шелла
		/// </summary>
		/// <param name="array">массив</param>
		public static void ShellSort(int[] array)
		{
			int gap = 1;
			while (gap < array.Length / 3)
			{
				gap = 3 * gap + 1;
			}

			while (gap >= 1)
			{
				for (int i = gap; i < array.Length; i++)
				{
					for (int j = i; j >= gap && array[j] < array[j - gap]; j -= gap)
					{
						Swap(array, j, j - gap);
					}
				}
				gap /= 3;
			}

		}

		/// <summary>
		/// сортировка слияния
		/// </summary>
		/// <param name="array">массив</param>
		public static void MergeSort(int[] array)
		{
			int[] aux = new int[array.Length];
			Sort(0, array.Length-1);

			void Sort(int low, int high)
			{
				if (high <= low)
					return;
				
				int mid = (high + low) / 2; //способ определния деления двух частей
				Sort(low, mid);  //левая сторона
				Sort(mid + 1, high); //правая сторона
				Merge(low, mid, high);
			}

			void Merge(int low, int mid, int high)
			{
				//если уже отсортированы
				if (array[mid] <= array[mid + 1])
					return;

				int i = low;
				int j = mid + 1;

				Array.Copy(array, low, aux, low, high - low + 1);

				for (int k = low; k <= high; k++)
				{
					if (i > mid)
						array[k] = aux[j++];
					else if (j > high)
						array[k] = aux[i++];
					else if (aux[j] < aux[i])
						array[k] = aux[j++];
					else
						array[k] = aux[i++];
				}
			}
		}

		/// <summary>
		/// быстрая сортировка 
		/// </summary>
		/// <param name="array">массив</param>
		public static void QuickSort(int[] array)
		{
			Sort(0, array.Length - 1);

			void Sort(int low, int high)
			{
				if(high <= low)
					return;

				int j = Partition(low, high);
				Sort(low,j-1); //левая сторона
				Sort(j + 1, high); //правая сторона
			}

			int Partition(int low, int high)
			{
				int i = low;
				int j = high + 1; //+1 - потому что будем в начале дикрементировать и тем самым получим последний элемент

				int pivot = array[low];

				while (true)
				{
					while (array[++i] < pivot)
					{
						if(i == high)
							break;
					}

					while (pivot < array[--j])
					{
						if(j == low)
							break;
					}

					if(i >= j)
						break;

					Swap(array,i,j);
				}
				Swap(array,low,j);
				return j;
			}
		}
	}
}
