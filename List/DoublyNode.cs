﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algorithms.List
{
	public class DoublyNode<T>
	{
		public DoublyNode<T> Previous { get; set; }
		public DoublyNode<T> Next { get; set; }

		public T Value { get; set; }

		public DoublyNode(T value)
		{
			Value = value;
		}
	}
}
