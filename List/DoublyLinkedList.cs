﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algorithms.List
{
	public class DoublyLinkedList<T>
	{
		public DoublyNode<T> Head { get; set; }
		public DoublyNode<T> Tail { get; set; }

		public void AddFirst(T value)
		{
			AddFirst(new DoublyNode<T>(value));
		}

		private void AddFirst(DoublyNode<T> node)
		{
			//сохранить головной узел
			DoublyNode<T> temp = Head;

			Head = node;

			Head.Next = temp;

			if (IsEmpty)
				Tail = node;
			else
			{
				//пример
				//сначала добавили: 1(Head) <-> 5 <-> 7 -> null
				//потом добавили: 3(Head) <-> 1 <-> 5 <-> 7 -> null
				temp.Previous = Head;
			}

			Count++;
		}

		public void AddLast(T value)
		{
			AddLast(new DoublyNode<T>(value));
		}

		private void AddLast(DoublyNode<T> node)
		{
			if (IsEmpty)
				Head = node;
			else
			{
				Tail.Next = node;
				node.Previous = Tail;
			}

			Tail = node;
			Count++;
		}

		public void RemoveFirst()
		{
			if (IsEmpty)
				throw new InvalidOperationException();
			//сдвигаем головной узел
			Head = Head.Next;

			Count--;

			if (IsEmpty)
				Tail = null;
			else
				Head.Previous = null;
		}

		public void RemoveLast()
		{
			if (IsEmpty)
				throw new InvalidOperationException();

			if(Count == 1)
			{
				Head = null;
				Tail = null;
			}
			else
			{
				Tail.Previous.Next = null; //удалили связь предпоследнего узла с последним
				Tail = Tail.Previous; //сдвигаем tail, теперь в списке предыдущий стал последним
			}

			Count--;
		}

		//написать метод который удаляет и добавляет в любоем место в списке

		public bool IsEmpty => Count == 0;
		public int Count { get; private set; }
	}
}
