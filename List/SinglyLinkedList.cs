﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Algorithms.List
{
	public class SinglyLinkedList<T> : IEnumerable<T>
	{
		public Node<T> Head { get; set; } //ссылка на головной узел
		public Node<T> Tail { get; set; } //ссылка на последний узел
		public int Count { get; set; } //отражает кол-во узлов в списке

		/// <summary>
		/// добавление узла в начало списка
		/// </summary>
		/// <param name="value"></param>
		public void AddFirst(T value)
		{
			AddFirst(new Node<T>(value));
		}

		/// <summary>
		/// реализация узла в начало списка
		/// </summary>
		/// <param name="node">узел</param>
		private void AddFirst(Node<T> node)
		{
			//сохранить текущую головную ноду
			Node<T> tmp = Head;
			
			//добавление новыой головной ноды
			Head = node;

			//ссылка новой головной ноды на старую
			Head.Next = tmp;

			Count++;

			if (Count == 1)
				Tail = Head;
		}

		/// <summary>
		/// добавление узла в конец списка
		/// </summary>
		/// <param name="value"></param>
		public void AddLast(T value)
		{
			AddLast(new Node<T>(value));
		}

		/// <summary>
		/// реализация добавления узла в конец списка
		/// </summary>
		/// <param name="node"></param>
		private void AddLast(Node<T> node)
		{
			//проверяем что список пуст или нет
			if (IsEmpty)
				Head = node;
			else
				Tail.Next = node; //добавили новый узел
			
			Tail = node; //проапдейтили tail

			Count++;
		}

		/// <summary>
		/// реализация удаления первого узла
		/// </summary>
		public void RemoveFirst()
		{
			if (IsEmpty)
				throw new InvalidOperationException();

			Head = Head.Next;

			if (Count == 1)
				Tail = null;

			Count--;
		}

		/// <summary>
		/// удаления последнего узла
		/// </summary>
		public void RemoveLast()
		{
			if (IsEmpty)
				throw new InvalidOperationException();

			if (Count == 1)
			{
				Head = Tail = null;
			}
			else
			{
				//сначала надо найти предпоследний узел
				var current = Head;
				while (current.Next != null)
				{
					current = current.Next;
				}

				current.Next = null;
				Tail = current;

				Count--;
			}
				
		}
		public bool IsEmpty => Count == 0;

		public IEnumerator<T> GetEnumerator()
		{
			Node<T> current = Head;
			while(current != null)
			{
				yield return current.Value;
				current = current.Next;
			}
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			throw new NotImplementedException();
		}

		
	}
}
