﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algorithms.List
{
	//узел
	public class Node<T>
	{
		public T Value { get; set; }  
		public Node<T> Next { get; set; } //указатель на другой узел(node)

		public Node(T value)
		{
			Value = value;
		}
	}
}
