﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Threading.Tasks.Dataflow;
using Algorithms.Heap;
using Algorithms.List;
using Algorithms.Tree;

namespace Algorithms
{
	class Program
	{
		static void Main(string[] args)
		{

			var heap = new MaxHeap<int>();
			heap.Insert(37);
			heap.Insert(24);
			heap.Insert(17);
			heap.Insert(28);
			heap.Insert(31);
			heap.Insert(29);
			heap.Insert(15);
			heap.Insert(12);
			heap.Insert(20);

			heap.Sort();

			foreach (var value in heap.Values())
			{
				Console.Write($"{value} , ");
			}


			//var bstTest = new BST<int>();
			//bstTest.Insert(37);
			//bstTest.Insert(24);
			//bstTest.Insert(17);
			//bstTest.Insert(28);
			//bstTest.Insert(31);
			//bstTest.Insert(29);
			//bstTest.Insert(15);
			//bstTest.Insert(12);
			//bstTest.Insert(20);

			//foreach (var i in bstTest.TraverseInOrder())
			//{
			//	Console.Write(i + " - ");
			//}

			//Console.WriteLine();
			//Console.WriteLine(bstTest.Min());
			//Console.WriteLine(bstTest.Max());
			//Console.WriteLine(bstTest.Get(20).Value);



			//foreach (var prime in Prime.Sieve(30))
			//{
			//	Console.WriteLine(prime + " " );
			//}

		}

		
		/// <summary>
		/// проверка стека списка
		/// </summary>
		static void LinkedStack()
		{
			var stack = new LinkedStack<int>();

			stack.Push(1);
			stack.Push(2);
			stack.Push(3);
			stack.Push(4);

			Console.WriteLine($"последний стэк = {stack.Peek()}");

			stack.Pop();

			Console.WriteLine($"после удаления = {stack.Peek()}");

			foreach (var i in stack)
			{
				Console.WriteLine(i);
			}
		}

		/// <summary>
		/// проверка стека массивов
		/// </summary>
		static void ArrayStack()
		{
			var stack = new ArrayStack<int>();

			stack.Push(1);
			stack.Push(2);
			stack.Push(3);
			stack.Push(4);

			Console.WriteLine($"последний стэк = {stack.Peek()}");

			stack.Pop();

			Console.WriteLine($"после удаления = {stack.Peek()}");

			foreach (var i in stack)
			{
				Console.WriteLine(i);
			}
		}

		/// <summary>
		/// метод в котором тестил виды сортировки
		/// </summary>
		static void TestingSort()
		{
			Sorting sort = new Sorting();
			Random r = new Random();
			int[] array = new int[10];


			Console.Write("Исходный масссив: ");
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = r.Next(30, 100);
				Console.Write(array[i] + " ");
			}

			Console.WriteLine("\n");

			//Console.Write("Отсортированный масссив (пузырька): ");
			//sort.BubbleSort(array);

			//Console.WriteLine("\n");

			//Console.Write("Отсортированный масссив (выборкой): ");
			//sort.SelectionSort(array);

			//Console.WriteLine("\n");

			//Console.Write("Отсортированный масссив (вставкой): ");
			//sort.InsertionSort(array);

			//Console.WriteLine("\n");

			//Console.Write("Отсортированный масссив (Шелла): ");
			//sort.ShellSort(array);

			//Console.WriteLine("\n");

			//Console.Write("Отсортированный масссив (слияния): ");
			//sort.MergeSort(array);

			//Console.WriteLine("\n");

			//Console.Write("Отсортированный масссив (быстрая): ");
			//sort.MergeSort(array);

			Console.WriteLine("\n");
			int n = 5;
			Console.WriteLine($"Факториал числа {n} равен {FactorialMethod(n)} (простой метод)");
			Console.WriteLine($"Факториал числа {n} равен {Recurse(n)} (рекурсия)");
		}

		/* скрыл так как логика такая пока не работает
		static void TestingLinkValue()
		{
			Node first = new Node() { Value = 5 };
			Node second = new Node() { Value = 1 };

			first.Next = second; //связали с второй nod-ой

			Node third = new Node() { Value = 3 };

			second.Next = third; //связали с третьей nod-ой

			PrintOutLinkedList(first);
		}
		static void PrintOutLinkedList(Node node)
		{
			//последний узел нет ссылки на след node и тем саммым значени next = null
			while (node != null)
			{
				Console.WriteLine(node.Value);

				node = node.Next;
			}
		}
		*/


		/// <summary>
		/// вычисление факторила через рекурсию
		/// </summary>
		/// <param name="number">число факториала</param>
		/// <returns>факториал</returns>
		static int Recurse(int number)
		{
			//1 вариант
			//int factorial = 1;
			//if(number == 0)
			//	return 1;

			//factorial *= Recurse(number - 1) * number;
			//return factorial;

			//2 вариант
			if (number == 0)
				return 1;

			return number * Recurse(number - 1);
		}

		/// <summary>
		/// вычисление факторила через обычный метод
		/// </summary>
		/// <param name="number">число факториала</param>
		/// <returns>факториал</returns>
		static int FactorialMethod(int number)
		{
			int factorial = 1;
			if (number == 0)
				return 1;

			for (int i = 1; i <= number; i++)
			{
				factorial *= i;
			}

			return factorial;
		}
	}
}
