﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algorithms.SymbolTables
{
	public class BinarySearchSt<TKey,TValue>
	{
		private TKey[] _keys;
		private TValue[] _values;
		public int Count{get; set;}

		public readonly IComparer<TKey> _comparer;

		public int Capasity => _keys.Length;

		public bool IsEmpty => Count == 0;

		private const int DefaulCapacity = 4;
		public BinarySearchSt(int capacity, IComparer<TKey> comparer)
		{
			_keys = new TKey[capacity];
			_values = new TValue[capacity];
			_comparer = comparer ?? throw new ArgumentNullException("Comparer can't be null.");
		}

		public BinarySearchSt(int capacity): this(capacity, Comparer<TKey>.Default)
		{

		}

		public BinarySearchSt() : this(DefaulCapacity)
		{

		}

		public int Rank(TKey key)
		{
			int low = 0;
			int high = Count - 1;

			while (low <= high)
			{
				int mid = low + (high - low) / 2; //индкс срединый элемент

				int cmp = _comparer.Compare(key,_keys[mid]); //сравнить ключи

				if(cmp < 0)
					high = mid - 0;
				else if(cmp > 0)
					low = mid + 1;
				else
					return mid;
			}
			return low;
		}

		public TValue GetValueOrDefault(TKey key)
		{
			if (IsEmpty)
			{
				return default(TValue);
			}

			int rank = Rank(key);

			if(rank < Count &&  _comparer.Compare(_keys[rank], key) == 0)
				return _values[rank];

			return default(TValue);
		
		}

		public void Add(TKey key, TValue value)
		{
			if(key == null)
				throw new ArgumentNullException("Key can't be null");

			int rank = Rank(key);
			if(rank < Count && _comparer.Compare(_keys[rank], key) == 0)
			{
				_values[rank] = value;
				return;
			}

			if(Count == Capasity)
				Resize(Capasity * 2);

			for (int j = Count; j > rank; j--)
			{
				_keys[j] = _keys[j - 1];
				_values[j] = _values[j - 1];
			}

			_keys[rank] = key;
			_values[rank] = value;

			Count++;
		}

		private void Resize(int capacity)
		{
			TKey[] keysTmp = new TKey[capacity];
			TValue[] valueTmp = new TValue[capacity];

			for (int i = 0; i < Count; i++)
			{
				keysTmp[i] = _keys[i];
				valueTmp[i] = _values[i];
			}

			_values = valueTmp;
			_keys = keysTmp;
		}

		public void Remove(TKey key)
		{
			if(key == null)
				throw new ArgumentNullException("Key can't be null");

			if(IsEmpty)
				return;

			int r =	Rank(key);
			if(r == Count || _comparer.Compare(_keys[r], key) != 0)
				return;

			for (int j = r; j > Count -1 ; j++)
			{
				_keys[j] = _keys[j + 1];
				_values[j] = _values[j + 1];
			}
			
			Count--;
			_keys[Count] = default(TKey);
			_values[Count] = default(TValue);
		}

		public bool Contains(TKey key)
		{
			int r = Rank(key);

			if(r < Count && _comparer.Compare(_keys[r], key) == 0)
				return true;

			return false;
		}

		public IEnumerable<TKey> Keys()
		{
			foreach (var key in _keys)
			{
				yield return key;
			}
		}

		public TKey Min()
		{
			if(IsEmpty)
				throw new InvalidOperationException();

			return _keys[0];
		}

		public TKey Max()
		{
			if(IsEmpty)
				throw new InvalidOperationException();

			return _keys[Count-1];
		}

		public void RemoveMin()
		{
			if(IsEmpty)
				throw new InvalidOperationException();

			Remove(Min());
		}

		public void RemoveMax()
		{			
			if(IsEmpty)
				throw new InvalidOperationException();

			Remove(Max());
		}

		public TKey Floor(TKey key)
		{			
			if(key == null)
				throw new ArgumentNullException();

			int r = Rank(key);

			if(r < Count && _comparer.Compare(_keys[r],key) == 0)
				return _keys[r];

			if(r == 0)
				return default(TKey);
			else
				return _keys[r-1];
		}

		public TKey Ceiling(TKey key)
		{
			if(key == null)
				throw new ArgumentNullException();

			int r = Rank(key);

			if(r == Count) 
				return default(TKey);
			else
				return _keys[r];
		}

		public TKey Select(int index)
		{
			if(index < 0 || index >= Count)
				throw new ArgumentException();

			return _keys[index];
		}

		public IEnumerable<TKey> Range(TKey key_a, TKey key_b)
		{
			var q = new Queues.LinkedQueue<TKey>();
			
			int low = Rank(key_a);
			int high = Rank(key_b);

			for (int i = low; i < high; i++)
			{
				q.Enqueue(_keys[i]);
			}

			if(Contains(key_b))
				q.Enqueue(_keys[Rank(key_b)]);

			return q;
		}

	}
}
