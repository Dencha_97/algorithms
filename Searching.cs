﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algorithms
{
	public class Searching
	{
		public static int RecursiveBinarySearch(int[] array, int value)
		{
			return IternalRecursiveBinarySearch(0, array.Length);
			int IternalRecursiveBinarySearch(int low, int high)
			{
				if(low >= high)
					return -1;

				int mid = (low + high) / 2;
				
				if(array[mid] == value)
					return mid;

				if(array[mid] < value)
					return IternalRecursiveBinarySearch(mid + 1, high);
				else
					return IternalRecursiveBinarySearch(low, mid);
			}
		}

		public static int BinerySearch(int[] array, int value)
		{
			int low = 0; //нижний
			int high = array.Length; //верхний

			while(low < high)
			{
				int mid = (low + high) / 2; //средний элемент
				
				if(array[mid] == value)
					return mid;
				
				if(array[mid] <value)
					low = mid + 1;
				else
					high = mid;
			}

			return -1; //ничег не нашли
		}
	}
}
