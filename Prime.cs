﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algorithms
{
	public class Prime
	{
		public static IEnumerable<int> Sieve(int max)
		{
			bool[] composit = new bool[max + 1];

			for (int p = 2; p <= max; p++)
			{
				if(composit[p]) continue;

				yield return p;

				for (int i = p * p; i <= max; i+=p)
				{
					composit[i] = true;
				}
			}
		}
	}
}
