﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Authentication;
using System.Text;
using System.Xml.Serialization;

namespace Algorithms.Heap
{
	public class MaxHeap<T> where T : IComparable<T>
	{
		private const int DefaultCapacity = 4;
		private T[] _heap;

		public int Count { get; set; }

		public bool IsFull => Count == _heap.Length;
		public bool IsEmpty => Count == 0;

		public MaxHeap() : this(DefaultCapacity)
		{
		}

		public MaxHeap(int capacity)
		{
			_heap = new T[capacity];
		}

		public void Insert(T value)
		{
			if (IsFull)
			{
				var newHeap = new T[_heap.Length * 2];
				Array.Copy(_heap, 0, newHeap, 0, _heap.Length);
				_heap = newHeap;
			}

			_heap[Count] = value;

			Swim(Count);
			Count++;
		}

		private void Swim(int index)
		{
			T newValue = _heap[index];

			//родительские эл-ты смещаем вниз
			while (index > 0 && IsParentLess(index))
			{
				_heap[index] = GetParent(index);
				index = ParentIndex(index);
			}

			_heap[index] = newValue;

			bool IsParentLess(int indexTwo)
			{
				return newValue.CompareTo(GetParent(indexTwo)) > 0;
			}
		}
		//возвращает значение
		private T GetParent(int index)
		{
			return _heap[ParentIndex(index)];
		}
		//возвращает индекс
		private int ParentIndex(int index)
		{
			return (index - 1) / 2;
		}

		public IEnumerable<T> Values()
		{
			for (int i = 0; i < Count; i++)
			{
				yield return _heap[i];
			}
		}

		public T Peek()
		{
			if (IsEmpty)
				throw new InvalidOperationException();

			return _heap[0];
		}

		public T Remove()
		{
			return Remove(0);
		}

		public T Remove(int index)
		{
			if (IsEmpty)
				throw new InvalidOperationException();

			T removedValue = _heap[index];
			_heap[index] = _heap[Count - 1];
			if (index == 0 || _heap[index].CompareTo(GetParent(index)) < 0) 
				Sink(index, Count - 1);
			else
				Swim(index);

			Count--;
			return removedValue;
		}

		private void Sink(int index, int lastHeapIndex)
		{
			
			while (index <= lastHeapIndex)
			{
				int leftChildIndex = LeftChildIndex(index);
				int rigthChildIndex = RigthChildIndex(index);

				if(leftChildIndex > lastHeapIndex)
					break;

				int childIndexToSwap = GetChildIndexToSwap(leftChildIndex, rigthChildIndex);

				if (SinkingIsLessThan(childIndexToSwap))
				{
					Exchange(index, childIndexToSwap);
				}
				else
				{
					break;
				}
			}

			int GetChildIndexToSwap(int leftChildIndex, int rigthChildIndex)
			{
				int childToSwap;
				if (rigthChildIndex > lastHeapIndex)
				{
					childToSwap = leftChildIndex;
				}
				else
				{
					int compareTo = _heap[leftChildIndex].CompareTo(_heap[rigthChildIndex]);
					childToSwap = compareTo > 0 ? leftChildIndex : rigthChildIndex;
				}

				return childToSwap;
			}

			bool SinkingIsLessThan(int childToSwap)
			{
				return _heap[index].CompareTo(_heap[childToSwap]) < 0;
			}
			
		}

		private int RigthChildIndex(int parentIndex)
		{
			return 2 * parentIndex + 1;
		}

		private int LeftChildIndex(int parentIndex)
		{
			return 2 * parentIndex + 2;
		}
		private void Exchange(int leftIndex, int rigthIndex)
		{
			T tmp = _heap[leftIndex];
			_heap[leftIndex] = _heap[rigthIndex];
			_heap[rigthIndex] = tmp;
		}

		public void Sort()
		{
			int lastHeapIndex = Count - 1;
			for (int i = 0; i < lastHeapIndex; i++)
			{
				Exchange(0, lastHeapIndex - i);
				Sink(0, lastHeapIndex - i - 1);
			}
		}


	}
}
